package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<Product> list = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        Product product = new Product(productName, price, quantity);

        if (price < 1 || quantity < 1 || list.size() >= PRODUCTS_LIMIT) return false;
        if (list.contains(product)) {
            int x = list.indexOf(product);
            if (list.get(x).getPrice() != product.price) return false;
            list.set(x, new Product(productName, price, list.get(x).getQuantity() + quantity));
        }
        return list.add(new Product(productName, price, quantity));
    }

    public boolean deleteProducts(String productName, int quantity) {
        if (quantity < 1) return false;
        if (list.stream()
                .anyMatch(product -> productName.equals(product.getProductName()))) {
            if (list.get(list.indexOf(new Product(productName, 0, 0))).quantity < quantity) return false;
            list.get(list.indexOf(new Product(productName, 0, 0))).reduceQuantity(quantity);
            return true;
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if (productName != null && list.contains(new Product(productName, 0, 0))) {
            return  (list.stream()
                    .filter(product -> productName.equals(product.getProductName()))
                    .findAny()
                    .orElse(null)).getQuantity();
        } else return 0;



    }

    public int getSumProductsPrices() {
        return list.stream().map(Product::getPrice).reduce(0, Integer::sum);
    }

    public int getProductPrice(String productName) {
        return list.get(list.indexOf(new Product(productName, 0, 0))).getPrice();
    }

    public List<String> getProductsNames() {
        return list.stream().map(Product::getProductName).collect(Collectors.toList());
    }
}
